﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float moveSpeed = 2;
    public Vector3 cameraPos;
    public Camera cam;

    private void Start()
    {
        cameraPos = gameObject.transform.position;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.W) && PersistentManagerScript.Instance.gameState != "Dialogue")
        {
            cameraPos = new Vector3(cameraPos.x, cameraPos.y += (moveSpeed / 10), cameraPos.z);
            transform.position = cameraPos;
        }
        if (Input.GetKey(KeyCode.S) && PersistentManagerScript.Instance.gameState != "Dialogue")
        {
            cameraPos = new Vector3(cameraPos.x, cameraPos.y -= (moveSpeed / 10), cameraPos.z);
            transform.position = cameraPos;
        }
        if (Input.GetKey(KeyCode.A) && PersistentManagerScript.Instance.gameState != "Dialogue")
        {
            cameraPos = new Vector3(cameraPos.x -= (moveSpeed / 10), cameraPos.y, cameraPos.z);
            transform.position = cameraPos;
        }
        if (Input.GetKey(KeyCode.D) && PersistentManagerScript.Instance.gameState != "Dialogue")
        {
            cameraPos = new Vector3(cameraPos.x += (moveSpeed / 10), cameraPos.y, cameraPos.z);
            transform.position = cameraPos;
        }
        if (Input.GetKey(KeyCode.Q) && cam.orthographicSize >= 1f && PersistentManagerScript.Instance.gameState != "Dialogue")
        {
            cam.orthographicSize -= 0.1f;
        }
        if (Input.GetKey(KeyCode.E) && cam.orthographicSize <= 10f && PersistentManagerScript.Instance.gameState != "Dialogue")
        {
            cam.orthographicSize += 0.1f;
        }
    }
}
