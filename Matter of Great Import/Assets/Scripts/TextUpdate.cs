﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextUpdate : MonoBehaviour
{
    public string[] charTexts;
    public int textNum;

    public GameObject choices;
    //public GameObject portrait; (run by Cole first)

    public Text text;

    private void Start()
    {
        textNum = 0;
        text.text = charTexts[0];
    }

    public void Clicked()
    {
        if (textNum == 0)
        {
            textNum += 1;
            text.text = charTexts[textNum];
            choices.SetActive(true);
        }
        else
        if(textNum > 1)
        {
            PersistentManagerScript.Instance.gameState = "Station";
            PersistentManagerScript.Instance.stateChange();
        }
    }

    public void Option1()
    {
        choices.SetActive(false);
        PersistentManagerScript.Instance.gameState = "placeRoom";
        PersistentManagerScript.Instance.stateChange();
    }

    public void Option2()
    {
        choices.SetActive(false);
        textNum = 3;
        textRefresh();
    }

    public void textRefresh()
    {
        text.text = charTexts[textNum];
    }
}
