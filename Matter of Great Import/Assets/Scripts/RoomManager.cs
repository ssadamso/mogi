﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoomManager : MonoBehaviour
{
    public Rooms room;

    public Text fullDescription;

    public GameObject CharTextBox;

    public string factionRelationship;
    public int roomRow;
    public int roomColumn;

    public Vector2Int roomLocation;

    public bool available;

    public GameObject ownPortal;

    public GameObject greenFade;
    public GameObject redFade;

    void Start()
    {
        available = false;
        FactionCheck();
        GetComponent<SpriteRenderer>().sprite = room.roomArt;
        roomLocation.y = roomRow;
        roomLocation.x = roomColumn;
        PersistentManagerScript.Instance.allRooms[roomColumn, roomRow] = room;
    }

    private void OnMouseOver()
    {
        colorCheck();

        if (Input.GetMouseButtonDown(0) && PersistentManagerScript.Instance.gameState == "Station")
        {
            TextCheck();
        }
        else
        if(Input.GetMouseButtonDown(0) && PersistentManagerScript.Instance.gameState == "placeRoom")
        {
            borderCheck();
            if (room.vacant == false)
            {
                fullDescription.text = "This room is taken!";
                PersistentManagerScript.Instance.roomTextActive = true;
            }
            else
            if(available == false)
            {
                fullDescription.text = "New rooms must be placed adjacent to old rooms!";
                PersistentManagerScript.Instance.roomTextActive = true;
            }
            else
            if (room.vacant == true && available == true)
            {
                room = PersistentManagerScript.Instance.potentialRoom[PersistentManagerScript.Instance.currentPotentialRoom];
                GetComponent<SpriteRenderer>().sprite = room.roomArt;
                ownPortal.SetActive(true);
                if (PersistentManagerScript.Instance.currentPotentialRoom == 0)
                {
                    PersistentManagerScript.Instance.interaction2.SetActive(true);
                    PersistentManagerScript.Instance.gameState = "Dialogue";
                    PersistentManagerScript.Instance.stateChange();
                }
                else
                    PersistentManagerScript.Instance.gameState = "Station";
                    PersistentManagerScript.Instance.stateChange();
                    if(PersistentManagerScript.Instance.currentPotentialRoom == 2)
                {
                    PersistentManagerScript.Instance.startPortrait();
                    branchCheck();
                }

                PersistentManagerScript.Instance.allRooms[roomColumn, roomRow] = room;
                //PersistentManagerScript.Instance.moneyGain += room.profit;
                PersistentManagerScript.Instance.bills += 25;
                PersistentManagerScript.Instance.billsText.text = "Bills: " + PersistentManagerScript.Instance.bills;
                PersistentManagerScript.Instance.currentPotentialRoom += 1;
                InvokeRepeating("portalAnim", 15.0f, 15.0f);
                greenFade.SetActive(false);
                redFade.SetActive(false);
                //CharTextBox.GetComponent<TextUpdate>().textNum = 2;
                //CharTextBox.GetComponent<TextUpdate>().textRefresh();

                if (room.faction == "Buyers R U")
                {
                    PersistentManagerScript.Instance.bRU.SetActive(true);
                }
                else
                if (room.faction == "Crime")
                {
                    PersistentManagerScript.Instance.crime.SetActive(true);
                }
            }
        }
    }

    private void OnMouseExit()
    {
        greenFade.SetActive(false);
        redFade.SetActive(false);
    }

    public void FactionCheck()
    {

        if (room.faction == "Law")
        {
            factionRelationship = PersistentManagerScript.Instance.lawRelationship;
        }
        else
        if(room.faction == "Crime")
        {
            factionRelationship = PersistentManagerScript.Instance.crimeRelationship;
        }
        else
        if(room.faction == "Buyers R U")
        {
            factionRelationship = PersistentManagerScript.Instance.merchantsRelationship;
        }
        else
        if(room.faction == "Alien")
        {
            factionRelationship = PersistentManagerScript.Instance.aliensRelationship;
        }
    }

    public void TextCheck()
    {
        if(PersistentManagerScript.Instance.roomTextActive == true && PersistentManagerScript.Instance.lastClickedRoom == roomLocation)
        {
            fullDescription.text = "";
            PersistentManagerScript.Instance.roomTextActive = false;
            PersistentManagerScript.Instance.lastClickedRoom = new Vector2(roomLocation.x, roomLocation.y);
        }
        else
        if(PersistentManagerScript.Instance.roomTextActive == true && PersistentManagerScript.Instance.lastClickedRoom != roomLocation)
        {
            PersistentManagerScript.Instance.lastClickedRoom = roomLocation;
            if (room.vacant == true)
            {
                fullDescription.text = "This room is vacant!";
            }
            else
            {
                fullDescription.text = "Name: " + room.roomName + "\n" + "Faction: " + room.faction + "\n" + "Profit: " + room.profit;
            }
        }
        else
        if(PersistentManagerScript.Instance.roomTextActive == false)
        {
            PersistentManagerScript.Instance.lastClickedRoom = roomLocation;
            if (room.vacant == true)
            {
                fullDescription.text = "This room is vacant!";
            }
            else
            {
                fullDescription.text = "Name: " + room.roomName + "\n" + "Faction: " + room.faction + "\n" + "Profit: " + room.profit;
            }
            PersistentManagerScript.Instance.roomTextActive = true;
        }
    }

    public void borderCheck()
    {
        if(PersistentManagerScript.Instance.allRooms[roomColumn - 1, roomRow] != null)
        {
            if (PersistentManagerScript.Instance.allRooms[roomColumn - 1, roomRow].faction != "None")
            {
                available = true;
            }
        }
        if (PersistentManagerScript.Instance.allRooms[roomColumn + 1, roomRow] != null)
        {
            if (PersistentManagerScript.Instance.allRooms[roomColumn + 1, roomRow].faction != "None")
            {
                available = true;
            }
        }
        if (PersistentManagerScript.Instance.allRooms[roomColumn, roomRow - 1] != null)
        {
            if (PersistentManagerScript.Instance.allRooms[roomColumn, roomRow - 1].faction != "None")
            {
                available = true;
            }
        }
        if (PersistentManagerScript.Instance.allRooms[roomColumn, roomRow + 1] != null)
        {
            if (PersistentManagerScript.Instance.allRooms[roomColumn, roomRow + 1].faction != "None")
            {
                available = true;
            }
        }
    }

    public void branchCheck()
    {
        if(room.faction == "Crime")
        {
            if (PersistentManagerScript.Instance.allRooms[roomColumn - 1, roomRow] != null)
            {
                if (PersistentManagerScript.Instance.allRooms[roomColumn - 1, roomRow].roomName == "Granville's Diner")
                {
                    PersistentManagerScript.Instance.currentInteraction += 1;
                    PersistentManagerScript.Instance.portraitNum += 1;
                }
            }
            if (PersistentManagerScript.Instance.allRooms[roomColumn + 1, roomRow] != null)
            {
                if (PersistentManagerScript.Instance.allRooms[roomColumn + 1, roomRow].roomName == "Granville's Diner")
                {
                    PersistentManagerScript.Instance.currentInteraction += 1;
                    PersistentManagerScript.Instance.portraitNum += 1;
                }
            }
            if (PersistentManagerScript.Instance.allRooms[roomColumn, roomRow - 1] != null)
            {
                if (PersistentManagerScript.Instance.allRooms[roomColumn, roomRow - 1].roomName == "Granville's Diner")
                {
                    PersistentManagerScript.Instance.currentInteraction += 1;
                    PersistentManagerScript.Instance.portraitNum += 1;
                }
            }
            if (PersistentManagerScript.Instance.allRooms[roomColumn, roomRow + 1] != null)
            {
                if (PersistentManagerScript.Instance.allRooms[roomColumn, roomRow + 1].roomName == "Granville's Diner")
                {
                    PersistentManagerScript.Instance.currentInteraction += 1;
                    PersistentManagerScript.Instance.portraitNum += 1;
                }
            }
        }
    }

    public void portalAnim()
    {
        ownPortal.GetComponent<Animator>().Play("normalPortalActive");
        PersistentManagerScript.Instance.money += room.profit;
        PersistentManagerScript.Instance.moneyText.text = "Money: " + PersistentManagerScript.Instance.money;
    }

    private void colorCheck()
    {
        if(PersistentManagerScript.Instance.gameState == "placeRoom")
        {
            borderCheck();
            if (room.vacant == true && available == true)
            {
                greenFade.SetActive(true);
            }
            else
            {
                redFade.SetActive(true);
            }
        }
    }
}
