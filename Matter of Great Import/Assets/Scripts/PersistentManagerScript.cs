﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PersistentManagerScript : MonoBehaviour
{
    public static PersistentManagerScript Instance { get; set; }

    public GameObject dialogueUI;

    public Rooms[] potentialRoom;

    public Rooms[,] allRooms = new Rooms[9, 5];

    public GameObject interaction2;

    public GameObject[] interactions;
    public int currentInteraction = 0;

    public string lawRelationship = "Neutral";
    public string crimeRelationship = "Neutral";
    public string merchantsRelationship = "Neutral";
    public string aliensRelationship = "Neutral";

    public int currentPotentialRoom;

    public bool roomTextActive = false;

    public Vector2 lastClickedRoom;

    public string gameState;

    public Text moneyText;
    public int money = 0;
    public bool moneyActive = false;
    public int moneyGain = 20;
    public float billRate = 0.02f;
    public int bills = 0;

    public GameObject roomPlaceText;

    public Sprite[] portraitSprites;
    public int portraitNum = 0;
    public GameObject portrait;

    public GameObject entryPortal;

    public GameObject bRU;
    public GameObject crime;

    public Slider bottomLine;
    public Text billsText;

    public GameObject end;

    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

        gameState = "Dialogue";
    }

    private void Start()
    {
        currentPotentialRoom = 0;
    }

    public void stateChange()
    {
        if(gameState == "Station")
        {
            dialogueUI.SetActive(false);
            roomPlaceText.SetActive(false);
        }
        else
        if(gameState == "Dialogue")
        {
            dialogueUI.SetActive(true);
            roomPlaceText.SetActive(false);
        }
        else
        if(gameState == "placeRoom")
        {
            dialogueUI.SetActive(false);
            roomPlaceText.SetActive(true);
        }
    }

    public void chooseRoom()
    {
        gameState = "placeRoom";
        stateChange();
    }

    public void returnStation()
    {
        gameState = "Station";
        stateChange();
    }

    public void startNewInteraction()
    {
        gameState = "Dialogue";
        stateChange();
        interactions[currentInteraction].SetActive(true);
        currentInteraction += 1;
        PersistentManagerScript.Instance.portraitNum += 1;
        portrait.SetActive(false);
    }

    public void skipInteraction()
    {
        currentInteraction += 1;
        portraitNum += 1;
    }

    public void startMoney()
    {
        moneyActive = true;
        InvokeRepeating("gainMoney", 7f, 7f);
        InvokeRepeating("loseMoney", 1f, 1f);
    }

    public void gainMoney()
    {
        moneyText.text = "Money: " + money;
        entryPortal.GetComponent<Animator>().Play("entryPortalActive");
    }

    public void loseMoney()
    {
        bottomLine.value += billRate;
        if (bottomLine.value >= 1)
        {
            bottomLine.value = 0;
            money -= bills;
            moneyText.text = "Money: " + money;
        }
    }

    public void startPortrait()
    {
        StartCoroutine(portraitTimer());
    }

    IEnumerator portraitTimer()
    {
        yield return new WaitForSeconds(7.0f);
        portrait.SetActive(true);
        portrait.GetComponent<Image>().sprite = portraitSprites[portraitNum];
    }

    public void endDemo()
    {
        end.SetActive(true);
    }

    public void donGood()
    {
        money += 100;
        moneyText.text = "Money: " + money;
        bRU.GetComponent<Slider>().value += 0.25f;
    }

    public void donBad()
    {
        bRU.GetComponent<Slider>().value -= 0.25f;
    }
}
