﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Room", menuName = "Room")]
public class Rooms : ScriptableObject
{
    public string roomName;
    public string faction;
    public string[] qualities;

    public int footTraffic;
    public int profit;
    public int cost;

    public bool vacant;

    public Sprite roomArt;
}
