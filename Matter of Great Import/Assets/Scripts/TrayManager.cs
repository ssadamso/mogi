﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrayManager : MonoBehaviour
{
    public bool spot1Empty;
    public bool spot2Empty;
    public bool spot3Empty;

    public Vector2[] portraitSpots;
}
